//
//  cardView.swift
//  recipSwiftUI
//
//  Created by Fan on 11/7/19.
//  Copyright © 2019 YiFan Xiao. All rights reserved.
//

import SwiftUI

struct cardView: View {
    @State private var tapped: Bool = false

    var landmark: Landmark
    
    var body: some View {
        VStack(alignment: .leading) {
            ZStack(alignment: .topTrailing) {
                Image(landmark.imageName)
                    .resizable()
                    .frame(minWidth: 0, maxWidth: .infinity, maxHeight: 300)
                Image(systemName: "xmark.circle")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .padding(30.0)
                    .foregroundColor(Color.white)
                    .onTapGesture {
                        self.tapped = false
                    }
                    .opacity(self.tapped ? 100: 0)
            }
            VStack(alignment: .leading) {
                Text(landmark.state)
                    .font(.headline)
                    .foregroundColor(Color.gray)
                
                Text(landmark.name)
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .multilineTextAlignment(.leading)
                Text(landmark.park)
                
                if self.tapped {
                    VStack {
                        Text("landmark.imageName")
                    }
                    .padding(.vertical)
                    Spacer()
                }
            }
            .padding()
        }
        .background(Color.white)
        .cornerRadius(15)
        .shadow(radius: 10)
        .offset(x: 0, y: 0)
        .onTapGesture {
            self.tapped = true
        }
        .animation(.default)
        .edgesIgnoringSafeArea(.all)
    }
}

struct cardView_Previews: PreviewProvider {
    static var previews: some View {
        cardView(landmark: landmarkData[0])
    }
}
