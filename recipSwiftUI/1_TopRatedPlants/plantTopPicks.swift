//
//  plantTopPicks.swift
//  recipSwiftUI
//
//  Created by Fan on 11/7/19.
//  Copyright © 2019 YiFan Xiao. All rights reserved.
//

import SwiftUI

struct plantTopPicks: View {
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                HStack {
                    Image(systemName: "archivebox")
                        .resizable()
                        .frame(width: 30, height: 30)
                        .foregroundColor(Color.black)
                    
                    Spacer()
                    
                    Image(systemName: "cart")
                        .resizable()
                        .frame(width: 30, height: 30)
                        .foregroundColor(Color.black)
                }
                
                Text("Top Pick")
                    .font(.largeTitle)
                    .fontWeight(.bold)
                    .padding(.vertical)
                
                HStack(alignment: .bottom) {
                    Text("Top")
                        .fontWeight(.heavy)
                        .foregroundColor(Color.black)
                    
                    Text("Outdoor")
                    Text("Indoor")
                    Text("Plant")
                }
                .font(.headline)
                .foregroundColor(Color.gray)
                .padding(.vertical)
            }
            .padding(.all)
            .padding(.horizontal, 10)
            
            ScrollView {
                VStack {
                    CardSlideView()
                    
                    VStack(alignment: .leading) {
                        Text("Description")
                            .font(.headline)
                            .padding(.vertical)
                        Text("Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?")
                    }
                    .padding(.all)
                }
                
            }
        }
        
    }
}

struct plantTopPicks_Previews: PreviewProvider {
    static var previews: some View {
        plantTopPicks()
    }
}

struct CardSlideView: View {
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(1 ..< 5){ _ in
                    plantCardView()
                        .frame(width: 300)
                        .aspectRatio(contentMode: .fit)
                        .padding(.horizontal, 20)
                }
            }
            .padding(.leading, 10)
        }
    }
}
