//
//  plantDetail.swift
//  recipSwiftUI
//
//  Created by Fan on 11/7/19.
//  Copyright © 2019 YiFan Xiao. All rights reserved.
//

import SwiftUI

struct plantDetail: View {
    var body: some View {
            ZStack(alignment: .bottom) {
                HeaderView()

                ZStack(alignment: .topTrailing) {
                    VStack(alignment: .leading, spacing: 20.0) {
                        
                        VStack(alignment: .leading, spacing: 10.0) {
                            Text("All to Know...")
                                .font(.title)
                                .fontWeight(.bold)
                            Text("Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.")
                        }
                        .padding(.top, 35)
                        
                        VStack(alignment: .leading, spacing: 10.0) {
                            Text("Detail")
                                .font(.title)
                                .fontWeight(.bold)
                            Text("Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.")
                        }
                        
                        Spacer()

                    }
                    .padding(.all, 30)
                    .background(Color.white)
                    .cornerRadius(25)
                    
                    Image("plant")
                        .resizable()
                        .frame(width: 300, height: 300)
                        .shadow(radius: 15)
                        .offset(x: 50, y: -250)
                }
                .offset(x: 0, y: 475)

            }
            .edgesIgnoringSafeArea(.all)
            
    }
}

struct plantDetail_Previews: PreviewProvider {
    static var previews: some View {
        plantDetail()
    }
}

struct HeaderView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 30.0) {
            HStack {
                Image(systemName: "arrow.left")
                    .resizable()
                    .frame(width: 30, height: 30)
                    .foregroundColor(Color.white)
                
                Spacer()
                
                Image(systemName: "cart")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .padding()
                    .foregroundColor(Color.white)
                    .background(Color.gray)
                    .blendMode(.lighten)
                    .clipShape(Circle())
            }
            
            VStack(alignment: .leading) {
                Text("MODUALR")
                    .font(.subheadline)
                Text("Ficus")
                    .font(.largeTitle)
                    .fontWeight(.heavy)
            }
            .padding(.bottom, 20)
            
            HStack {
                VStack(alignment: .leading, spacing: 20.0) {
                    VStack(alignment: .leading) {
                        Text("PRICE")
                            .font(.subheadline)
                        Text("$30")
                            .font(.title)
                            .fontWeight(.heavy)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("SIZE")
                            .font(.subheadline)
                        Text("Small")
                            .font(.title)
                            .fontWeight(.heavy)
                    }
                    
                    VStack {
                        Image(systemName: "cart.badge.plus")
                            .resizable()
                            .frame(width: 30, height: 25)
                            .foregroundColor(Color.white)
                            .padding()
                    }
                    .background(Color.black)
                    .clipShape(Circle())
                }
                
            }
            
            Spacer()
        }
        .padding(30)
        .background(Color.green)
    }
}
