//
//  plantCardView.swift
//  recipSwiftUI
//
//  Created by Fan on 11/7/19.
//  Copyright © 2019 YiFan Xiao. All rights reserved.
//

import SwiftUI

struct plantCardView: View {
    var body: some View {
        VStack {
            VStack {
                HStack() {
                    Spacer()
                    VStack(alignment: .leading) {
                        Text("Price")
                            .font(.subheadline)
                        Text("$25")
                            .font(.title)
                            .fontWeight(.heavy)
                    }
                    .foregroundColor(Color.white)
                }
                .padding()
                
                Image("plant")
                    .resizable()
                    .frame(width: 180, height: 180)
                    .aspectRatio(contentMode: .fill)
                
                HStack(alignment: .center) {
                    VStack(alignment: .leading) {
                        Text("Home Living")
                            .font(.subheadline)
                        Text("Aloe Ficus")
                            .font(.title)
                            .fontWeight(.heavy)
                    }
                    .foregroundColor(Color.white)

                    Spacer()
                    
                    HStack(spacing: 20.0) {
                        Image(systemName: "hand.thumbsup")
                        Image(systemName: "hand.thumbsdown")
                        Image(systemName: "square.and.arrow.up")
                        Image(systemName: "info.circle")
                    }
                    .foregroundColor(Color.white)
                }
                .padding()
                .padding(.bottom, 35)


            }
            .background(Color.green)
            .cornerRadius(15)
            
            VStack {
                VStack {
                    Image(systemName: "hand.thumbsup")
                        .resizable()
                        .frame(width: 25, height: 25)
                        .foregroundColor(Color.white)
                }
                .padding(25)
            }
            .background(Color.gray)
            .clipShape(Circle())
            .padding(.top, -60)
        }
    }
}

struct plantCardView_Previews: PreviewProvider {
    static var previews: some View {
        plantCardView()
    }
}
