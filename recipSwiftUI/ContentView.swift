//
//  ContentView.swift
//  recipSwiftUI
//
//  Created by Fan on 11/7/19.
//  Copyright © 2019 YiFan Xiao. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Parks")
                .font(.largeTitle)
                .fontWeight(.heavy)
                .multilineTextAlignment(.leading)
            ScrollView {
                VStack(spacing: 12.0) {
                    ForEach(landmarkData) { landmark in
                        cardView(landmark: landmark)
                            .frame(height: 425)
                            .padding()
                    }
                }
            }
        }
        .padding()
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
